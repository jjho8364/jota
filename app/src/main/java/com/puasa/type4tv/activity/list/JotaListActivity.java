package com.puasa.type4tv.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.puasa.type4tv.R;
import com.puasa.type4tv.activity.VideoPlayerActivity;
import com.puasa.type4tv.adapter.MidBtnAdapter;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class JotaListActivity extends Activity {
    private String TAG = " JotaListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetListView getListView = null;
    private String baseUrl = "";

    private TextView storyView;
    private String playerUrl = "";
    private String nextUrl = "";

    private ListView btnListView;
    private ImageView poster;

    private int adsCnt = 0;
    private String firstUrl = "";

    private String referrer = "";
    private GetPlayer getPlayer;

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=1500;
    private long mLastClickTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mubi_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        referrer = intent.getStringExtra("listUrl");
        firstUrl = intent.getStringExtra("firstUrl");

        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        storyView = (TextView)findViewById(R.id.tv_movie_story);
        poster = (ImageView) findViewById(R.id.iv_list_poster);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(JotaListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();


                imgUrl = doc.select("#bo_v_con img").attr("src");

                //////////////// episode contents ////////////
                String[] storyArr =  doc.select("#bo_v_con").text().split("열어보기");
                if(storyArr.length > 1) {
                    story = storyArr[1];
                } else {
                    story = storyArr[0];
                }

                ///// get video link /////
                Elements elements = doc.select("#movie_bt a");

                for(Element element: elements) {
                    String btnTitle = element.text();
                    String btnVideoUrl = element.attr("href");

                    if(btnTitle.contains("다운로드")) continue;

                    btnTextArr.add(btnTitle);
                    btnVideoUrlArr.add(btnVideoUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            storyView.setText(story);

            if(imgUrl == null || imgUrl.equals("")){
                poster.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(JotaListActivity.this).load(imgUrl).into(poster);
            }

            ///////// set button list ////////////
            btnListView.setAdapter(new MidBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    long currentClickTime= SystemClock.uptimeMillis();
                    long elapsedTime=currentClickTime-mLastClickTime;
                    mLastClickTime=currentClickTime;
                    // 중복 클릭인 경우
                    if(elapsedTime<=MIN_CLICK_INTERVAL){
                        return;
                    }

                    nextUrl = btnVideoUrlArr.get(position);

                    getPlayer = new GetPlayer();
                    getPlayer.execute();
                }
            });

            mProgressDialog.dismiss();

        }
    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(JotaListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(15000).referrer(referrer).get();

                String str[] = doc.select("body script").first().data().split("\"");
                playerUrl = str[1];

                Log.d(TAG, "playerUrl : " + playerUrl);

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(JotaListActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }

}
