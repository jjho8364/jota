package com.puasa.type4tv.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.puasa.type4tv.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SplashActivity extends Activity {
    private String TAG = "SplashActivity - ";
    private GetStatus getStatus = null;
    private String splashInfo3 = "";
    private String tistoryUrl = "";
    private int tistoryLastPage = 0;
    private int tistoryRndPage = 0;
    private TextView tvSplashInfo;

    private String baseUrl = "";
    private String[] baseUrlArr = new String[]{
            "https://teddyzaffran.tistory.com/1"
            //,"https://findfoodbaru.tistory.com/1"
            //,"https://sherlong.tistory.com/1"
            //,"https://koreaskincare.tistory.com/1"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        tvSplashInfo = (TextView)findViewById(R.id.splash_info3);

        baseUrl = baseUrlArr[(int)(Math.random() * baseUrlArr.length)];

        Log.d(TAG, "selected baseUrl : " + baseUrl);

        getStatus = new GetStatus();
        getStatus.execute();
    }

    public class GetStatus extends AsyncTask<Void, Void, Void> {

        String appStatus = "";
        String fragment01Url = "";
        String fragment02Url = "";
        String fragment03Url = "";
        String fragment04Url = "";
        String fragment05Url = "";
        String fragment06Url = "";
        String fragment07Url = "";
        String fragment08Url = "";
        String fragment09Url = "";
        String fragment10Url = "";
        String fragment11Url = "";
        String fragment12Url = "";
        String fragLiveTv = "";
        String fragLiveTvEnter = "";
        String maintenanceImgUrl = "";
        String closedImgUrl = "";
        String nextAppUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document tistoryDoc = null;
            Document doc = null;

            try {
                tistoryDoc = Jsoup.connect(baseUrl).timeout(15000).get();

                String strHtml = tistoryDoc.select("b").text();
                doc = Jsoup.parse(strHtml);

                appStatus = doc.select(".type4").text();
                //appStatus = doc.select(".test").text();
                Log.d(TAG, "appStatus : " + appStatus);

                splashInfo3 = doc.select(".splash_info5").text();

                String[] tempStrCoder = doc.select(".coderstorage").text().split(",");
                tistoryUrl = tempStrCoder[0];
                tistoryLastPage = Integer.parseInt(tempStrCoder[1].trim());
                Log.d(TAG, "tistoryUrl : " + tistoryUrl);
                Log.d(TAG, "tistoryLastPage : " + tistoryLastPage);

                // 랜덤 페이지 가져오기
                tistoryRndPage = ((int)(Math.random() * (tistoryLastPage-1))) + 2;

                if(appStatus.equals("1")){
                    fragment01Url = doc.select(".fragmentQooSearch").text();
                    fragment02Url = doc.select(".fragmentQooDrama").text();
                    fragment03Url = doc.select(".fragmentQooEnter").text();
                    fragment04Url = doc.select(".fragmentQooSisa").text();
                    fragment05Url = doc.select(".fragmentQooDaqu").text();
                    fragment06Url = doc.select(".fragmentJotaDrama").text();
                    fragment07Url = doc.select(".fragmentJotaEnter").text();
                    fragment08Url = doc.select(".fragmentJotaSisa").text();
                    fragment09Url = doc.select(".fragmentJotaMovie").text();
                    fragment10Url = doc.select(".fragmentJotaMid").text();
                    fragment11Url = doc.select(".fragmentJotaJoongd").text();
                } else if(appStatus.equals("2")){
                    maintenanceImgUrl = doc.select(".maintenance").text();
                    fragment01Url = doc.select(".fragmentQooSearch").text();
                    fragment02Url = doc.select(".fragmentQooDrama").text();
                    fragment03Url = doc.select(".fragmentQooEnter").text();
                    fragment04Url = doc.select(".fragmentQooSisa").text();
                    fragment05Url = doc.select(".fragmentQooDaqu").text();
                    fragment06Url = doc.select(".fragmentJotaDrama").text();
                    fragment07Url = doc.select(".fragmentJotaEnter").text();
                    fragment08Url = doc.select(".fragmentJotaSisa").text();
                    fragment09Url = doc.select(".fragmentJotaMovie").text();
                    fragment10Url = doc.select(".fragmentJotaMid").text();
                    fragment11Url = doc.select(".fragmentJotaJoongd").text();
                } else if(appStatus.equals("3")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".newappurl").text();
                } else if(appStatus.equals("9")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".mid.site").text();
                } else {
                    maintenanceImgUrl = doc.select(".maintenance").text();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tvSplashInfo.setText(splashInfo3);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplication(), MainActivity.class);

                    intent.putExtra("appStatus", appStatus);

                    if(appStatus.equals("1")){
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                        intent.putExtra("fragment07Url", fragment07Url);
                        intent.putExtra("fragment08Url", fragment08Url);
                        intent.putExtra("fragment09Url", fragment09Url);
                        intent.putExtra("fragment10Url", fragment10Url);
                        intent.putExtra("fragment11Url", fragment11Url);
                        intent.putExtra("tistoryUrl", tistoryUrl);
                        intent.putExtra("tistoryRndPage", tistoryRndPage+"");

                    } else if(appStatus.equals("2")){
                        intent.putExtra("maintenance", maintenanceImgUrl);
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                        intent.putExtra("fragment07Url", fragment07Url);
                        intent.putExtra("fragment08Url", fragment08Url);
                        intent.putExtra("fragment09Url", fragment09Url);
                        intent.putExtra("fragment10Url", fragment10Url);
                        intent.putExtra("fragment11Url", fragment11Url);
                        intent.putExtra("tistoryUrl", tistoryUrl);
                        intent.putExtra("tistoryRndPage", tistoryRndPage+"");
                    } else if(appStatus.equals("3")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else if(appStatus.equals("9")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else {
                        intent.putExtra("maintenance", maintenanceImgUrl);
                    }
                    finish();
                    startActivity(intent);
                }
            }, 3000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getStatus != null){
            getStatus.cancel(true);
        }
    }
}
